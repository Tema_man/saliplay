package com.cherryman.sailplay;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

/**
 * Some utility class to replace boilerplate code
 *
 * @author Artemii Vishnevskii
 * @author Temaa.mann@gmail.com
 * @since 18.03.2016.
 */
public class Permissions {

	public static final int REQUEST_CAMERA = 0x0FF2;
	public static final int REQUEST_NAVIGATION = 0x0FF3;
	public static final int REQUEST_STORAGE = 0x0FF4;

	public static final String[] CAMERA = {
			Manifest.permission.CAMERA
	};

	public static final String[] NAVIGATION = {
			Manifest.permission.ACCESS_COARSE_LOCATION,
			Manifest.permission.ACCESS_FINE_LOCATION,
	};

	public static final String[] STORAGE = {
			Manifest.permission.WRITE_EXTERNAL_STORAGE,
			Manifest.permission.READ_EXTERNAL_STORAGE
	};

	/**
	 * Requests is specified permissions
	 *
	 * @param activity    {@link Activity} in which context request will be performed
	 * @param permissions an array of permissions to be checked
	 */
	public static void request(Activity activity, int requestCode, String... permissions) {
		ActivityCompat.requestPermissions(activity, permissions, requestCode);
	}

	/**
	 * Same as {@link #request(Activity, int, String...)} but for fragment
	 *
	 * @param fragment    a {@link Fragment} to check permission
	 * @param requestCode a request code
	 * @param permissions an array of permissions to check
	 */
	public static void request(Fragment fragment, int requestCode, String... permissions) {
		if (!fragment.isAdded()) return;
		fragment.requestPermissions(permissions, requestCode);
	}

	/**
	 * Checks is requested permissions are granted
	 *
	 * @param ctx         {@link Context} for access to android system permissions checking
	 * @param permissions permissions names for check
	 * @return true if all permissions are granted, false otherwise.
	 */
	public static boolean check(Context ctx, String... permissions) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) return true;
		for (String permission : permissions) {
			if (ContextCompat.checkSelfPermission(ctx, permission)
					!= PackageManager.PERMISSION_GRANTED) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Checks a bunch of permission results to is there granted. May be called from
	 * {@link Activity#onRequestPermissionsResult(int, String[], int[])}
	 * parameters are identical except of request code
	 *
	 * @param permissions  array of permissions names
	 * @param grantResults array with grant results of requested permissions
	 * @return true if all permission results is grantResults array has a {@link
	 * PackageManager#PERMISSION_GRANTED} value, false otherwise.
	 */
	public static boolean hasAll(String[] permissions, int[] grantResults) {
		if (permissions.length != grantResults.length) return false;
		for (int i = 0; i < permissions.length; i++) {
			if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Checks that only specified permissions was granted
	 *
	 * @param permissions  an array of permissions from {@link Activity#onRequestPermissionsResult(int,
	 *                     String[], int[])}
	 * @param grantResults array of grant results from {@link Activity#onRequestPermissionsResult(int,
	 *                     String[], int[])}
	 * @param names        names of permission to check
	 * @return true if all of permissions in 'names' was granted, false otherwise
	 */
	public static boolean hasSeveral(String[] permissions, int[] grantResults, String... names) {
		if (permissions.length != grantResults.length) return false;
		if (names == null || names.length == 0) return false;
		for (int i = 0; i < permissions.length; i++) {
			for (String name : names) {
				if (permissions[i].equals(name) && grantResults[i] != PackageManager.PERMISSION_GRANTED) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Opens an application Settings screen
	 *
	 * @param context {@link Context} instance
	 */
	public static void openAppSettings(Context context) {
		Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
		Uri uri = Uri.fromParts("package", context.getPackageName(), null);
		intent.setData(uri);
		context.startActivity(intent);
	}

	public static boolean checkNavigationAccess(Activity activity) {
		return check(activity, NAVIGATION);
	}
}