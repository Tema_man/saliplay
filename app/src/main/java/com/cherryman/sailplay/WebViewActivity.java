package com.cherryman.sailplay;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.webkit.PermissionRequest;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class WebViewActivity extends Activity {

	private static final String TAG = "WebViewActivity";
	private WebView mWebView;


	private static final int REQUEST_SELECT_FILE = 100;
	private static final int FILECHOOSER_RESULT_CODE = 1;

	private ValueCallback<Uri> mUploadMessage;
	public ValueCallback<Uri[]> uploadMessage;

	private static final int PERMISSIONS_REQUEST_CODE = 0x0121;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web_view);

		mWebView = (WebView) findViewById(R.id.awv_webview);

		prepareWebView();

		if (Permissions.check(this, Constants.PERMISSIONS)) {
			mWebView.loadUrl(Constants.LINK);
		} else {
			Permissions.request(this, PERMISSIONS_REQUEST_CODE, Constants.PERMISSIONS);
		}
	}

	@Override
	public void onRequestPermissionsResult(
			int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if (Permissions.hasAll(permissions, grantResults)) {
			mWebView.loadUrl(Constants.LINK);
		} else {
			showPermissionsRationale();
		}
	}

	private void showPermissionsRationale(){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(R.string.label_permissions_rationale);
		builder.setNeutralButton(R.string.label_settings, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
				Uri uri = Uri.fromParts("package", getPackageName(), null);
				intent.setData(uri);
				startActivity(intent);
				finish();
			}
		});
		builder.show();
	}

	@Override
	public void onBackPressed() {
		if (mWebView.canGoBack()) {
			mWebView.goBack();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			if (requestCode == REQUEST_SELECT_FILE) {
				if (uploadMessage == null) return;
				uploadMessage.onReceiveValue(WebChromeClient.FileChooserParams
						.parseResult(resultCode, intent));

				uploadMessage = null;
			}
		} else if (requestCode == FILECHOOSER_RESULT_CODE) {
			if (mUploadMessage == null) return;
			Uri result = (intent == null || resultCode != RESULT_OK) ? null : intent.getData();
			mUploadMessage.onReceiveValue(result);
			mUploadMessage = null;
		} else {
			Toast.makeText(this, "Failed to Upload Image", Toast.LENGTH_LONG).show();
		}
	}

	@SuppressLint("SetJavaScriptEnabled")
	private void prepareWebView() {
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.getSettings().setLoadWithOverviewMode(true);
		mWebView.getSettings().setUseWideViewPort(true);
		mWebView.getSettings().setDomStorageEnabled(true);
		mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);

		mWebView.getSettings().setAllowFileAccessFromFileURLs(true);
		mWebView.getSettings().setAllowFileAccess(true);
		mWebView.getSettings().setAllowContentAccess(true);
		mWebView.getSettings().setAllowUniversalAccessFromFileURLs(true);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			mWebView.getSettings().setMediaPlaybackRequiresUserGesture(false);
		}

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			WebView.setWebContentsDebuggingEnabled(true);
		}

		mWebView.setWebViewClient(new MyWebViewClient());
		mWebView.setWebChromeClient(new MyWebChromeClient());
	}

	private class MyWebChromeClient extends WebChromeClient {

		@Override
		public void onPermissionRequest(final PermissionRequest request) {
			WebViewActivity.this.runOnUiThread(new Runnable() {
				@Override
				@TargetApi(Build.VERSION_CODES.LOLLIPOP)
				public void run() {
					request.grant(request.getResources());
				}
			});
			Log.d(TAG, "onPermissionRequest:");
		}

		@TargetApi(Build.VERSION_CODES.LOLLIPOP)
		public boolean onShowFileChooser(WebView mWebView, ValueCallback<Uri[]> filePathCallback,
		                                 WebChromeClient.FileChooserParams fileChooserParams) {
			if (uploadMessage != null) {
				uploadMessage.onReceiveValue(null);
				uploadMessage = null;
			}

			uploadMessage = filePathCallback;

			Intent intent = fileChooserParams.createIntent();
			try {
				startActivityForResult(intent, REQUEST_SELECT_FILE);
			} catch (ActivityNotFoundException e) {
				uploadMessage = null;
				Toast.makeText(WebViewActivity.this, "Cannot Open File Chooser", Toast.LENGTH_LONG).show();
				return false;
			}
			return true;
		}

		public void openFileChooser(ValueCallback<Uri> uploadMsg) {
			mUploadMessage = uploadMsg;
			Intent i = new Intent(Intent.ACTION_GET_CONTENT);
			i.addCategory(Intent.CATEGORY_OPENABLE);
			i.setType("image/*");
			WebViewActivity.this.startActivityForResult(Intent.createChooser(i, "Image Chooser"), FILECHOOSER_RESULT_CODE);
		}

		public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
			openFileChooser(uploadMsg);
		}

		public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
			openFileChooser(uploadMsg);
		}
	}

	private class MyWebViewClient extends WebViewClient {

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			return processUri(Uri.parse(url));
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				return processUri(request.getUrl());
			}
			return true;
		}

		private boolean processUri(Uri uri) {
			switch (uri.getScheme()) {
				case "tel":
					Intent tel = new Intent(Intent.ACTION_DIAL, uri);
					startActivity(Intent.createChooser(tel, "Call..."));
					return true;
				case "mailto":
					Intent mail = new Intent(Intent.ACTION_SENDTO, uri);
					startActivity(Intent.createChooser(mail, "Send email..."));
					return true;
				default:
					return false;
			}
		}
	}
}
