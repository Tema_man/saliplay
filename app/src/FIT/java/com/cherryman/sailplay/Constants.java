package com.cherryman.sailplay;


import android.Manifest;

public class Constants {

	public static final String LINK = "https://sailplay.ru/mobile/?partner_id=1646#!/";
	public static final String[] PERMISSIONS = {
			Manifest.permission.CAMERA,
			Manifest.permission.READ_EXTERNAL_STORAGE};

}
